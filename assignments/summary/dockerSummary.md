## Docker Basics

## What is Docker 

Docker is a platform which packages an application and all its dependencies together in the form of containers. This containerization aspect ensures that the application works in any environment.

 Each and every application runs on separate containers and has its own set of dependencies & libraries. This makes sure that each application is independent of other applications, giving developers surety that they can build applications that will not interfere with one another.

 ## Containers and Virtual machines

 A container runs natively on Linux and shares the kernel of the host machine with other containers. It runs a discrete process, taking no more memory than any other executable, making it lightweight.

By contrast, a virtual machine (VM) runs a full-blown “guest” operating system with virtual access to host resources through a hypervisor. In general, VMs incur a lot of overhead beyond what is being consumed by your application logic.
   
   Below is the image show difference between containers and virtual machines

![docker](https://techglimpse.com/wp-content/uploads/2016/03/Container-vs-VMs.jpg)

## Docker images and containers

A Docker container is nothing but a running process, with some added encapsulation features applied to it in order to keep it isolated from the host and from other containers.
 
An Docker image includes everything needed to run an application - the code or binary, runtimes, dependencies, and any other filesystem objects required.

![docker img](https://geekflare.com/wp-content/uploads/2019/07/dockerfile-697x270.png)

## Docker Architecture

Docker follows Client-Server architecture, which includes the three main components that are Docker Client, Docker Host, and Docker Registry.

![dock archi](https://static.javatpoint.com/docker/images/docker-architecture.png)

#### Docker Client
   Docker client uses commands and REST APIs to communicate with the Docker Daemon (Server).

#### Docker Host

Docker Host is used to provide an environment to execute and run applications. It contains the docker daemon, images, containers, networks, and storage.

#### Docker Registry

Docker Registry manages and stores the Docker images.
There are two types of registries in the Docker -

**Pubic Registry** - Public Registry is also called as Docker hub.

**Private Registry** - It is used to share images within the enterprise.

## Docker dockerfile
A Dockerfile is a text document that contains commands that are used to assemble an image. We can use any command that call on the command line. Docker builds images automatically by reading the instructions from the Dockerfile.

The docker build command is used to build an image from the Dockerfile. You can use the -f flag with docker build to point to a Dockerfile anywhere in your file system.

## Docker commands

### Containers

Use `docker container my_command`

`create` — Create a container from an image.

`start` — Start an existing container.

`run` — Create a new container and start it.

`ls` — List running containers.

`inspect` — See lots of info about a container.

`logs` — Print logs.

`stop` — Gracefully stop running container.

`kill` —Stop main process in container abruptly.

`rm`— Delete a stopped container.

### Images

Use `docker image my_command`

`build` — Build an image.

`push` — Push an image to a remote registry.

`ls` — List images.

`history` — See intermediate image info.

`inspect` — See lots of info about an image, including the layers.

`rm` — Delete an image.

### Misc

`docker version` — List info about your Docker Client and Server versions.

`docker login` — Log in to a Docker registry.

`docker system prune` — Delete all unused containers, unused networks, and dangling images.

![dock cycle](https://trishnag.files.wordpress.com/2016/04/docker1.jpg?w=809)

## Docker Example

 `docker run hello-world`

Hello from Docker.
This message shows that your installation appears to be working correctly.

## Docker features

Although Docker provides lots of features, we are listing some major features which are given below.


- Easy and Faster Configuration
- Increase productivity
- Application Isolation
- Swarm
- Routing Mesh
- Services
- Security Management