## Git

Git is a distributed version control system, which is software that allows developers to maintain a complete history of every file in a project. However, you don't need to be a developer to use Git with your website. For example, you can use Git to maintain a history of all of your website files: every time a file is changed, a version of the change is recorded in the Git software. If necessary, you can always go back to any version and restore it.

![git image](https://www.cloudsavvyit.com/thumbcache/0/0/5b8ff1fbf94a3ecddbaa8db6b389c09a/p/uploads/2019/10/e713ed70-1.png)

## Why Git

- Git is developed to ensure the security and integrity of content being version controlled.
- Git is the most widely used version control system. It has maximum projects among all the version control systems.
- Almost All operations of Git can be performed locally; this is a significant reason for the use of Git.
- There are many public projects available on the GitHub. We can collaborate on those projects and show our creativity to the world.

![why git](https://static.javatpoint.com/tutorial/git/images/why-git.png)


## Git Repositories

A Git repository contains the history of a collection of files starting from a certain directory. The process of copying an existing Git repository via the Git tooling is called cloning. After cloning a repository the user has the complete repository with its history on his local machine. Of course, Git also supports the creation of new repositories.

![git repos](https://media.geeksforgeeks.org/wp-content/uploads/20191121165133/repository1.jpg)


## Git working Tree   
![tree](https://ieee.nitk.ac.in/blog/assets/img/A-Dive-into-Git_Directory/three_areas_git.png)

A local repository provides at least one collection of files which originate from a certain version of the repository. This collection of files is called the working tree. The user can change the files in the working tree by modifying existing files and by creating and removing files.
A file in the working tree of a Git repository can have different states. These states are the following:

- untracked: the file is not tracked by the Git repository. This means that the file never staged nor committed.
- tracked: committed and not staged
- staged: staged to be included in the next commit
- dirty / modified: the file has changed but the change is not staged

## Git Workflow

**Branch** 
A branch is a version of the repository that diverges from the main working project.

**Checkout**
In Git, the term checkout is used for the act of switching between different versions of a target entity. The git checkout command is used to switch between branches in a repository.

**Clone**
The git clone is a Git command-line utility. It is used to make a copy of the target repository or clone it.

**Fetch**
It is used to fetch branches and tags from one or more other repositories, along with the objects necessary to complete their histories.

**Head**
HEAD is the representation of the last commit in the current checkout branch. We can think of the head like a current branch. When you switch branches with git checkout, the HEAD revision changes, and points the new branch.

**Master**
EAD is the representation of the last commit in the current checkout branch. We can think of the head like a current branch. When you switch branches with git checkout, the HEAD revision changes, and points the new branch.

![work](https://i.redd.it/nm1w0gnf2zh11.png)


## Git command

- `git config` - This command sets the author name and email address respectively to be used with your commits.

- `git init` - This command is used to start a new repository.

- `git clone` - This command is used to obtain a repository from an existing URL.

- `git add` - This command adds a file to the staging area.
 
- `git commit` - This command records or snapshots the file permanently in the version history.
 
- `git diff` - This command shows the file differences which are not yet staged.

- `git reset` - This command unstages the file, but it preserves the file contents.
 
- `git status` - This command lists all the files that have to be committed.
 
- `git rm` - This command deletes the file from your working directory and stages the deletion.
 
- `git log` - This command is used to list the version history for the current branch.

- `git show` - This command shows the metadata and content changes of the specified commit.

- `git tag` - This command is used to give tags to the specified commit.
 
- `git branch` - This command lists all the local branches in the current repository.
 
- `git checkout` - This command is used to switch from one branch to another.
 
- `git push` - This command sends the committed changes of master branch to your remote repository.
 
- `git pull` - This command fetches and merges changes on the remote server to your working directory.
 
- `git stash` - This command temporarily stores all the modified tracked files.

## Git Features


![featurs](https://static.javatpoint.com/tutorial/git/images/features-of-git.jpg)


